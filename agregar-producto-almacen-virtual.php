<?
include_once("./app/sistema/rutas.php");
?>
<!DOCTYPE html>
<html lang="en">    
    <head>
    <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Agregar producto almacen virtual</title>

        <!-- CSS de Bootstrap -->
        <link href="<?=$raizComponentes?>bootstrap-5.0.0-beta3-dist/css/bootstrap.min.css" rel="stylesheet" media="screen">

        <!--Estilos Propios-->
        <link href="<?=$raizEstilos?>productos.css" rel="stylesheet" media="screen">

        <!-- Componentes de JS -->
        <script src="<?=$raizComponentes?>jquery/jquery-3.6.0.min.js"></script>
        <link href="<?=$raizComponentes?>bootstrap-5.0.0-beta3-dist/js/bootstrap.min.js" rel="stylesheet" media="screen">
        <!--Propias-->
        <script src="<?=$raizJs?>funciones.js"></script>
    </head>
    <body class="body-content">
        
        <div class="container" style="margin-top: 25px;">
        <div class="alert alert-light" role="alert"><h4 style="text-align: center;">Agregar producto a almacen virtual</div>
            <div class="row">
                <div class="col col-lg-4">

                </div>
                <div class="col col-lg-4" style="background-color: white;">
                    <?php
                        require_once($_SERVER['DOCUMENT_ROOT']."/Productos/app/controladores/cProducto.php");
                    ?>
                </div>
                <div class="col col-lg-4">

                </div>
            </div>
        </div>
    </body>
</html>
