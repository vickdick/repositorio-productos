<?php

include_once ("Bd.php");

class Almacen extends Bd{

    function obtenerAlmacenes(){
        $this->abrirBD();
        $query = "SELECT id_almacen, nombre_almacen, tipo FROM almacen";
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }
   
    function obtenerAlmacenesFisicos(){
        $this->abrirBD();
        $query = "SELECT id_almacen, nombre_almacen FROM almacen
                    WHERE tipo = true";
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }

    function obtenerAlmacenesVirtuales(){
        $this->abrirBD();
        $query = "SELECT id_almacen, nombre_almacen FROM almacen
                    WHERE tipo = false";
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }

    function getAlmacenFisico($idProducto){
        $this->abrirBD();
        $query = "SELECT id_almacen FROM existencias
                    WHERE id_producto = $idProducto";
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }



}