<?php

include_once ("Bd.php");

class Producto extends Bd{
    private $idProducto;

    function ultimoIdProducto(){
        $this->abrirBD();
        $query = "SELECT max(id_producto) from producto";
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }

    function eliminarExistencias($idProducto){
        $this->abrirBD();
        $query = "DELETE from existencias WHERE id_producto = $idProducto";
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }

    function eliminarExistenciasFisicas($idProducto){
        $this->abrirBD();
        $query = "DELETE from existencias WHERE id_producto = $idProducto AND id_almacen =";
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }

    function eliminarProducto($idProducto){
        $this->abrirBD();
        $query = "DELETE from producto WHERE id_producto = $idProducto";
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }

    function obtenerProductos(){
        $this->abrirBD();
        $query = "SELECT prod.id_producto, prod.sku, prod.descripcion, prod.marca, prod.color, prod.precio, sum(exist.existencias) 
                        FROM producto as prod, existencias as exist
                            WHERE prod.id_producto = exist.id_producto
                            GROUP BY prod.id_producto
                            ORDER BY sum(exist.existencias) DESC";
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }

    function obtenerProductosAlmacenesFisicos(){
        $this->abrirBD();
        $query = "SELECT prod.id_producto, prod.sku, prod.descripcion, prod.marca, prod.color, prod.precio, exist.id_producto, sum(exist.existencias), alm.tipo, alm.nombre_almacen, alm.id_almacen 
                    FROM producto as prod, existencias as exist, almacen as alm
                    WHERE (prod.id_producto = exist.id_producto) 
                        AND (exist.id_almacen = alm.id_almacen) 
                        AND alm.tipo = true
                        
                    GROUP BY prod.id_producto, alm.tipo, exist.id_producto, exist.existencias, alm.nombre_almacen, alm.id_almacen
                    ORDER BY sum(exist.existencias) DESC"; 
                       
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado; 
    }

    function obtenerProductosAlmacenesVirtuales(){
        $this->abrirBD();
        $query = "SELECT prod.id_producto, prod.sku, prod.descripcion, prod.marca, prod.color, prod.precio, exist.id_producto, sum(exist.existencias), alm.tipo, alm.nombre_almacen, alm.id_almacen 
                    FROM producto as prod, existencias as exist, almacen as alm
                    WHERE (prod.id_producto = exist.id_producto) 
                        AND (exist.id_almacen = alm.id_almacen) 
                        AND alm.tipo = false
                        
                    GROUP BY prod.id_producto, alm.tipo, exist.id_producto, exist.existencias, alm.nombre_almacen, alm.id_almacen
                    ORDER BY sum(exist.existencias) DESC"; 
                       
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado; 
    }

    function obtenerProductosFisicos($idProducto){
        $this->abrirBD();
        $query = "SELECT prod.id_producto, prod.sku, prod.descripcion, prod.marca, prod.color, prod.precio, exist.id_producto, exist.existencias, alm.tipo, alm.nombre_almacen, alm.id_almacen 
                    FROM producto as prod, existencias as exist, almacen as alm
                    WHERE (prod.id_producto = exist.id_producto) 
                        AND (exist.id_almacen = alm.id_almacen) 
                        AND alm.tipo = true
                        AND prod.id_producto = $idProducto
                    GROUP BY prod.id_producto, alm.tipo, exist.id_producto, exist.existencias, alm.nombre_almacen, alm.id_almacen
                    ORDER BY prod.marca ASC"; 
                       
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado; 
    }

    function obtenerProductosVirtuales($idProducto){
        $this->abrirBD();
        $query = "SELECT prod.id_producto, prod.sku, prod.descripcion, prod.marca, prod.color, prod.precio, exist.id_producto, exist.existencias, alm.tipo, alm.nombre_almacen, alm.id_almacen 
                    FROM producto as prod, existencias as exist, almacen as alm
                    WHERE (prod.id_producto = exist.id_producto) 
                        AND (exist.id_almacen = alm.id_almacen) 
                        AND alm.tipo = false
                        AND prod.id_producto = $idProducto
                    GROUP BY prod.id_producto, alm.tipo, exist.id_producto, exist.existencias, alm.nombre_almacen, alm.id_almacen
                    ORDER BY prod.marca ASC";         
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado; 
    }
    
    function getProducto($idProducto){
        $this->abrirBD();
        $query = "SELECT id_producto, sku, descripcion, marca, color, precio
		            FROM producto   
			        WHERE id_producto = $idProducto";         
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }

    function getProductoExistenciasFisicas($idProducto){
        $this->abrirBD();
        $query = "SELECT p.id_producto, e.existencias, a.id_almacen FROM producto as p, existencias as e, almacen as a
                    WHERE (p.id_producto = e.id_producto)
                        AND (e.id_almacen = a.id_almacen)
                            AND p.id_producto = $idProducto
                            AND a.tipo = true;";         
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }

    function getProductoExistenciasVirtuales($idProducto){
        $this->abrirBD();
        $query = "SELECT p.id_producto, e.existencias, a.id_almacen FROM producto as p, existencias as e, almacen as a
                    WHERE (p.id_producto = e.id_producto)
                        AND (e.id_almacen = a.id_almacen)
                            AND p.id_producto = $idProducto
                            AND a.tipo = false;";         
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }

    function actualizarProducto($array){
        $this->abrirBD();
        $query = "UPDATE producto
                    SET sku = '".$array[1]."',
                     descripcion = '".$array[2]."',
                     marca = '".$array[3]."',
                     color = '".$array[4]."',
                     precio = '".$array[5]."'
                     WHERE id_producto = $array[0]";                   
        $resultado = pg_query($query);
        $this->cerrarBD();
        return $resultado;
    }

    function actualizarProductoExistencias($idProducto, $idAlmacen, $existenciasProducto){
        $this->abrirBD();
        $query = "UPDATE existencias
                    SET existencias = $existenciasProducto
                        WHERE id_producto = $idProducto AND id_almacen = $idAlmacen";
        $resultado = pg_query($query);
        $this->cerrarBD();

        return $resultado;    
    }

    function guardarProducto($array){

        $this->abrirBD();
        $query = "INSERT INTO producto (sku, descripcion, marca, color, precio) 
                        VALUES
                        ('".$array[1]."', '".$array[2]."', '".$array[3]."', '".$array[4]."', '".$array[5]."')";
        $resultado = pg_query($query);
        $this->cerrarBD();

        return $resultado;
    }

    function guardarExistencias($array){
        $this->abrirBD();
        $query = "INSERT INTO existencias (id_producto, id_almacen, existencias)
                        VALUES
                        ((SELECT id_producto FROM producto WHERE sku = '".$array[1]."'), $array[0], $array[6])";
        $resultado = pg_query($query);
        var_dump($resultado);
        $this->cerrarBD();

        return $resultado;
    }

}