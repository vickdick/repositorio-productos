<?php

require_once($_SERVER['DOCUMENT_ROOT']."/Productos/app/vistas/vProducto.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Productos/app/entidades/Producto.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Productos/app/entidades/Almacen.php");

class cProducto{
    public $opcion;

    function __construct($orden){
        $this->opcion = $orden;


        /**creamos las instancias necesarias**/
        $objVistaProducto = new vProducto();

        //Switch para hacer las diferentes peticiones de los productos
        switch($this->opcion){
            case 'muestraProductos':
                $producto = new Producto();
                $resProductos = pg_fetch_all($producto->obtenerProductos());

                $vProductos = new vProducto();
                $vProductos->muestraProductos($resProductos);
            break;
            case 'muestraProductosFisicos':
                $producto = new Producto();
                $resProductos = pg_fetch_all($producto->obtenerProductosAlmacenesFisicos());

                $vProductos = new vProducto();
                $vProductos->muestraProductosFisicos($resProductos);
            break;
            case 'muestraProductosVirtuales':
                $producto = new Producto();
                $resProductos = pg_fetch_all($producto->obtenerProductosAlmacenesVirtuales());

                $vProductos = new vProducto();
                $vProductos->muestraProductosVirtuales($resProductos);
            break;        
            case 'mostrarExistenciasFisicas':
                $id = $_REQUEST['id'];
                
                $producto = new Producto();
                $resProductos = pg_fetch_all($producto->obtenerProductosFisicos($id));
                $vProductos = new vProducto();
                $vProductos->muestraProductosAlmacenFisico($resProductos);
            break;  
            case 'mostrarExistenciasVirtuales':
                $id = $_REQUEST['id'];
                
                $producto = new Producto();
                $resProductos = pg_fetch_all($producto->obtenerProductosVirtuales($id));
                $vProductos = new vProducto();
                $vProductos->muestraProductosAlmacenVirtual($resProductos);
            break;  
            case 'editarProducto':
                $id = $_REQUEST['id'];

                $producto = new Producto();
                $resProductos = pg_fetch_object($producto->getProducto($id));
                $vProductos = new vProducto();
                $vProductos->muestraProductosEdit($resProductos);
            break;   
            case 'editarProductoExistencias':
                $id = $_REQUEST['id'];
                
                $producto = new Producto();
                $resProductos = pg_fetch_object($producto->getProductoExistenciasFisicas($id));
                $vProductos = new vProducto();
                $vProductos->muestraProductosEditExistencias($resProductos); 
            break;
            case 'editarProductoExistenciasVirtuales':
                $id = $_REQUEST['id'];
                
                $producto = new Producto();
                $resProductos = pg_fetch_object($producto->getProductoExistenciasVirtuales($id));
                $vProductos = new vProducto();
                $vProductos->muestraProductosEditExistenciasVirtuales($resProductos); 
            break;    
            case 'actualizarProductoExistenciasFisicas':
                $idProducto = $_REQUEST['idProducto'];
                $idAlmacen = $_REQUEST['idAlmacen'];
                $existenciasProducto = $_REQUEST['existenciasProducto'];    

                $producto = new Producto();
                $resProducto = $producto->actualizarProductoExistencias($idProducto, $idAlmacen, $existenciasProducto);

                if($resProducto){
                    header('Location:/Productos/index.php?orden=muestraProductosFisicos&mensaje=actualizar');
                    exit();
                    //echo "actualizado";
                }
            case 'actualizarProductoExistenciasVirtuales':
                $idProducto = $_REQUEST['idProducto'];
                $idAlmacen = $_REQUEST['idAlmacen'];
                $existenciasProducto = $_REQUEST['existenciasProducto'];    

                $producto = new Producto();
                $resProducto = $producto->actualizarProductoExistencias($idProducto, $idAlmacen, $existenciasProducto);

                if($resProducto){
                    header('Location:/Productos/index.php?orden=muestraProductosVirtuales&mensaje=actualizar');
                    exit();
                    //echo "actualizado";
                }
            break;        
            break;    
            case 'actualizarProducto':
                $idProducto = $_REQUEST['idProducto'];
                $codigoProducto = $_REQUEST['codigoProducto'];
                $descProducto = $_REQUEST['descripcionProducto'];
                $marcaProducto = $_REQUEST['marcaProducto'];
                $colorProducto = $_REQUEST['colorProducto'];
                $precioProducto = $_REQUEST['precioProducto'];

                $arrayProducto = array($idProducto, $codigoProducto, $descProducto, $marcaProducto, $colorProducto, $precioProducto);
                
                $producto = new Producto();
                $resProducto = $producto->actualizarProducto($arrayProducto);

                if($arrayProducto){
                    header('Location:/Productos/index.php?orden=muestraProductos&mensaje=actualizar');
                    exit();
                    //echo "actualizado";
                }
            break;
            case 'eliminarProducto':
                $idProducto = $_REQUEST['id'];
                
                $producto = new Producto();

                $almacen = new Almacen();
                $idAlmacen = $almacen->getAlmacenFisico($idProducto);
                $resProducto = $producto->eliminarExistencias($idProducto);

                if($resProducto){
                    $resProducto2 = $producto->eliminarProducto($idProducto);
                    if($resProducto2){
                        header('Location:/Productos/index.php?orden=muestraProductos&mensaje=eliminar');
                        exit();    
                    }
                }

            break;  
            case 'eliminarProductoFisico':
                $idProducto = $_REQUEST['id'];

                $producto = new Producto();
                $resProducto = $producto->eliminarExistenciasFisicas($idProducto);

                if($resProducto){
                    $resProducto2 = $producto->eliminarProducto($idProducto);
                    if($resProducto2){
                        header('Location:/Productos/index.php?orden=muestraProductos&mensaje=eliminar');
                        exit();    
                    }
                }
            break;      
            case 'agregarProductoXalmacen':
                //$idAlmacen = $_REQUEST['idAlmacen'];
                
                $almacenes = new Almacen();
                $resAlmacenes = pg_fetch_all($almacenes->obtenerAlmacenes());

                $vProducto = new vProducto();
                $vProducto->muestraProductoAgregar($resAlmacenes);
            break;    
            case 'guardarProducto':
                //$idAlmacen = $_REQUEST['idAlmacen'];
                $codigoProducto = $_REQUEST['codigoProducto'];
                $descProducto = $_REQUEST['descripcionProducto'];
                $marcaProducto = $_REQUEST['marcaProducto'];
                $colorProducto = $_REQUEST['colorProducto'];
                $precioProducto = $_REQUEST['precioProducto'];
                $existenciasProducto = $_REQUEST['numeroExistencias'];
                $idAlmacen = $_REQUEST['inputAlmacen'];

                $arrayProducto = array($idAlmacen, $codigoProducto, $descProducto, $marcaProducto, $colorProducto, $precioProducto, $existenciasProducto);
                
            
                $producto = new Producto();
                $resProducto1 = $producto->guardarProducto($arrayProducto);

                if($resProducto1){
                    $resProducto2 = $producto->guardarExistencias($arrayProducto);
                    if($resProducto2){
                        header('Location:/Productos/index.php?orden=muestraProductos&mensaje=guardar');
                        exit();    
                    }
                }

            break;  
            case 'agregarProductoAlmacenFisico':
                $almacenes = new Almacen();
                $resAlmacenes = pg_fetch_all($almacenes->obtenerAlmacenesFisicos());

                $vProducto = new vProducto();
                $vProducto->muestraProductoAgregarAlmacenFisicos($resAlmacenes);
            break; 
            case 'guardarProductoAlmacenFisico':
                $codigoProducto = $_REQUEST['codigoProducto'];
                $descProducto = $_REQUEST['descripcionProducto'];
                $marcaProducto = $_REQUEST['marcaProducto'];
                $colorProducto = $_REQUEST['colorProducto'];
                $precioProducto = $_REQUEST['precioProducto'];
                $existenciasProducto = $_REQUEST['numeroExistencias'];
                $idAlmacen = $_REQUEST['inputAlmacen'];

                $array = array($idAlmacen, $codigoProducto, $descProducto,  $marcaProducto, $colorProducto, $precioProducto, $existenciasProducto);
                //print_r($array);

                $producto = new Producto();
                $resProducto1 = $producto->guardarProducto($array);

                if($resProducto1){
                    $resProducto2 = $producto->guardarExistencias($array);
                    if($resProducto2){
                        header('Location:/Productos/index.php?mensaje=guardar');
                        exit();    
                    }
                }

            break;       
            case 'agregarProductoAlmacenVirtual':
                $almacenes = new Almacen();
                $resAlmacenes = pg_fetch_all($almacenes->obtenerAlmacenesVirtuales());

                $vProducto = new vProducto();
                $vProducto->muestraProductoAgregarAlmacenVirtual($resAlmacenes);
            break;
            case 'guardarProductoAlmacenVirtual':
                
                $codigoProducto = $_REQUEST['codigoProducto'];
                $descProducto = $_REQUEST['descripcionProducto'];
                $marcaProducto = $_REQUEST['marcaProducto'];
                $colorProducto = $_REQUEST['colorProducto'];
                $precioProducto = $_REQUEST['precioProducto'];
                $existenciasProducto = $_REQUEST['numeroExistencias'];
                $idAlmacen = $_REQUEST['inputAlmacen'];

                $array = array($idAlmacen, $codigoProducto, $descProducto,  $marcaProducto, $colorProducto, $precioProducto, $existenciasProducto);
                //print_r($array);

                $producto = new Producto();
                $resProducto1 = $producto->guardarProducto($array);

                if($resProducto1){
                    $resProducto2 = $producto->guardarExistencias($array);
                    if($resProducto2){
                        header('Location:/Productos/index.php?mensaje=guardar');
                        exit();    
                    }
                }
            break;        
            default;
            break;    
        }
    }

}
if (isset($_REQUEST['orden'])) {
    new cProducto($_REQUEST['orden']);
}

?>
