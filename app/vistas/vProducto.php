<?php
require_once($_SERVER['DOCUMENT_ROOT']."/Productos/app/entidades/Producto.php");

class vProducto{

    function muestraProductos($resProductos){
        
        ?>
            <ol class="list-group list-group-numbered">
                <?
                    for($i = 0; $i < sizeof($resProductos); $i++){
                        ?>
                                <li class="list-group-item d-flex justify-content-between align-items-start">
                                    <div class="ms-2 me-auto">
                                    <div class="fw-bold"><?=$resProductos[$i]['marca']?></div>
                                    <?=$resProductos[$i]['descripcion']?> $<?=$resProductos[$i]['precio']?> 
                                    </div>
                                    <div class="btn-group" role="group" aria-label="Basic outlined example" style="margin-right: 20px;">
                                        <button type="button" class="btn btn-outline-primary" onclick="editarProducto(<?=$resProductos[$i]['id_producto']?>)">Editar</button>
                                        <button type="button" class="btn btn-outline-danger" onclick="eliminarProducto(<?=$resProductos[$i]['id_producto']?>)">Eliminar</button></button>
                                        <!--<button type="button" class="btn btn-outline-primary" onclick="mostrarExistenciasVirtuales(<?=$resProductos[$i]['id_producto']?>)">Existencias (Almacen Virtual)</button>-->
                                    </div>
                                    <div style="width: 50px;">
                                        <span class="badge bg-primary rounded-pill"><?=$resProductos[$i]['sum']?></span>
                                    </div>
                                </li>
                        <?
                    }
                ?>  
            </ol>    
        <?
    }

    function muestraProductosFisicos($resProductos){
        ?>
            <ol class="list-group list-group-numbered">
                <?
                    for($i = 0; $i < sizeof($resProductos); $i++){
                        ?>
                                <li class="list-group-item d-flex justify-content-between align-items-start">
                                    <div class="ms-2 me-auto">
                                    <div class="fw-bold"><?=$resProductos[$i]['marca']?> (<?=$resProductos[$i]['nombre_almacen']?>)</div>
                                    <?=$resProductos[$i]['descripcion']?> $<?=$resProductos[$i]['precio']?> 
                                    </div>
                                    <div class="btn-group" role="group" aria-label="Basic outlined example" style="margin-right: 20px;">
                                        <button type="button" class="btn btn-outline-primary" onclick="editarExistenciasFisicas(<?=$resProductos[$i]['id_producto']?>)">Editar</button>
                                        <!--<button type="button" class="btn btn-outline-danger" onclick="eliminarProductoFisico(<?=$resProductos[$i]['id_producto']?>)">Eliminar</button></button>
                                        <button type="button" class="btn btn-outline-primary" onclick="mostrarExistenciasVirtuales(<?=$resProductos[$i]['id_producto']?>)">Existencias (Almacen Virtual)</button>-->
                                    </div>
                                    <div style="width: 50px;">
                                        <span class="badge bg-primary rounded-pill"><?=$resProductos[$i]['sum']?></span>
                                    </div>
                                </li>
                        <?
                    }
                ?>  
            </ol>    
        <?
    }

    function muestraProductosVirtuales($resProductos){
        ?>
            <ol class="list-group list-group-numbered">
                <?
                    for($i = 0; $i < sizeof($resProductos); $i++){
                        ?>
                                <li class="list-group-item d-flex justify-content-between align-items-start">
                                    <div class="ms-2 me-auto">
                                    <div class="fw-bold"><?=$resProductos[$i]['marca']?> (<?=$resProductos[$i]['nombre_almacen']?>)</div>
                                    <?=$resProductos[$i]['descripcion']?> $<?=$resProductos[$i]['precio']?> 
                                    </div>
                                    <div class="btn-group" role="group" aria-label="Basic outlined example" style="margin-right: 20px;">
                                        <button type="button" class="btn btn-outline-primary" onclick="editarExistenciasVirtuales(<?=$resProductos[$i]['id_producto']?>)">Editar</button>
                                        <!--<button type="button" class="btn btn-outline-primary" onclick="mostrarExistenciasFisicas(<?=$resProductos[$i]['id_producto']?>)">Existencias (Almacen Físico)</button></button>
                                        <button type="button" class="btn btn-outline-primary" onclick="mostrarExistenciasVirtuales(<?=$resProductos[$i]['id_producto']?>)">Existencias (Almacen Virtual)</button>-->
                                    </div>
                                    <div style="width: 50px;">
                                        <span class="badge bg-primary rounded-pill"><?=$resProductos[$i]['sum']?></span>
                                    </div>
                                </li>
                        <?
                    }
                ?>  
            </ol>    
        <?
    }

    function muestraProductosAlmacenFisico($resProductos){
        //var_dump($resProductos);
        if(!empty($resProductos)){
            ?>
                <ol class="list-group list-group-numbered">
                    <?
                        for($i = 0; $i < sizeof($resProductos); $i++){
                            ?>
                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                        <div class="fw-bold"><?=$resProductos[$i]['marca']?></div>
                                        <?=$resProductos[$i]['descripcion']?>
                                        </div>
                                        <div class="btn-group" role="group" aria-label="Basic outlined example" style="margin-right: 20px;">
                                            <button type="button" class="btn btn-outline-primary" onclick="editarProducto(<?=$resProductos[$i]['id_producto']?>)">Editar</button>
                                            <button type="button" class="btn btn-outline-primary" onclick="agregarProductoAlmacen(<?= $resProductos[$i]['id_almacen']?>)">Agregar nuevo producto al almacen <?=$resProductos[$i]['nombre_almacen']?></button></button>
                                            <button type="button" class="btn btn-outline-primary" onclick="agregarProductoAlmacenFisico()">Agregar nuevo producto a otro almacen fisico</button></button>
                                       </div>
                                        <div style="width: 50px;">
                                            <span class="badge bg-primary rounded-pill"><?=$resProductos[$i]['existencias']?></span>
                                        </div>
                                    </li>
                            <?
                        }
                    ?>  
                </ol>    
            <?
        }else{
            ?>
                <div class="alert alert-danger" role="alert">
                    No hay productos en almacen físico para este producto
                </div>
            <?
        }
    }

    function muestraProductosAlmacenVirtual($resProductos){
        
        if(!empty($resProductos)){
            ?>
                <ol class="list-group list-group-numbered">
                    <?
                        for($i = 0; $i < sizeof($resProductos); $i++){
                            ?>
                                    <li class="list-group-item d-flex justify-content-between align-items-start">
                                        <div class="ms-2 me-auto">
                                        <div class="fw-bold"><?=$resProductos[$i]['marca']?></div>
                                        <?=$resProductos[$i]['descripcion']?>
                                        </div>
                                        <div class="btn-group" role="group" aria-label="Basic outlined example" style="margin-right: 20px;">
                                            <button type="button" class="btn btn-outline-primary" onclick="editarProducto(<?=$resProductos[$i]['id_producto']?>)">Editar</button>
                                            <button type="button" class="btn btn-outline-primary" onclick="agregarProductoAlmacen(<?= $resProductos[$i]['id_almacen']?>)">Agregar nuevo producto al almacen <?=$resProductos[$i]['nombre_almacen']?></button></button>
                                            <button type="button" class="btn btn-outline-primary" onclick="agregarProductoAlmacenVirtual()">Agregar nuevo producto a otro almacen virtual</button></button>
                                        </div>
                                        <div style="width: 50px;">
                                            <span class="badge bg-primary rounded-pill"><?=$resProductos[$i]['existencias']?></span>
                                        </div>
                                    </li>
                            <?
                        }
                    ?>  
                </ol>    
            <?
        }else{
            ?>
                <div class="alert alert-danger" role="alert">
                    No hay productos en almacen físico para este producto
                </div>
            <?
        }
    }

    function muestraProductosEdit($resProductos){
        //var_dump($resProductos);
        ?>
            <form action="app/controladores/cProducto.php?orden=actualizarProducto" method="post" style="margin: 30px;"> 
            
                <div class="form-group">
                    <label for="codigoProducto"><strong>Codigo de producto</strong></label>
                    <input type="hidden" class="form-control" name = "idProducto" id="idProducto" value="<?=$resProductos->id_producto?>">
                    <input type="text" class="form-control" name = "codigoProducto" id="codigoProducto" value="<?=$resProductos->sku?>">
                </div>
                <div class="form-group">
                    <label for="descripcionProducto"><strong>Descripcion de producto</strong></label>
                    <input type="text" class="form-control" name ="descripcionProducto" id="descripcionProducto" value="<?=$resProductos->descripcion?>">
                </div>
                <div class="form-group">
                    <label for="marcaProducto"><strong>Marca de producto</strong></label>
                    <input type="text" class="form-control" name="marcaProducto" id="marcaProducto" value="<?=$resProductos->marca?>">
                </div>
                <div class="form-group">
                    <label for="colorProducto"><strong>Color de producto</strong></label>
                    <input type="text" class="form-control" name ="colorProducto" id="colorProducto" value="<?=$resProductos->color?>">
                </div>
                <div class="form-group">
                    <label for="precioProducto"><strong>Precio de producto</strong></label>
                    <input type="text" class="form-control" name ="precioProducto" id="precioProducto" value="<?=$resProductos->precio?>">
                </div>
                <div style="align-items: center;">
                    <button type="send" class="btn btn-success" style="margin-top: 10px;">Actualizar</button>
                </div>
            </form>
        <?
    }

    function muestraProductosEditExistencias($resProductos){
        //var_dump($resProductos);    
        ?>
            <form action="app/controladores/cProducto.php?orden=actualizarProductoExistenciasFisicas" method="post" style="margin: 30px;"> 
            
                <div class="form-group">
                    <label for="existenciasProducto"><strong>Existencias de producto</strong></label>
                    <input type="hidden" class="form-control" name = "idProducto" id="idProducto" value="<?=$resProductos->id_producto?>">
                    <input type="hidden" class="form-control" name = "idAlmacen" id="idAlmacen" value="<?=$resProductos->id_almacen?>">
                    <input type="number" class="form-control" name ="existenciasProducto" id="existenciasProducto" value="<?=$resProductos->existencias?>">
                </div>
                <div style="align-items: center;">
                    <button type="send" class="btn btn-success" style="margin-top: 10px;">Actualizar</button>
                </div>
            </form>
        <?
    }

    function muestraProductosEditExistenciasVirtuales($resProductos){
        ?>
            <form action="app/controladores/cProducto.php?orden=actualizarProductoExistenciasVirtuales" method="post" style="margin: 30px;"> 
            
                <div class="form-group">
                    <label for="existenciasProducto"><strong>Existencias de producto</strong></label>
                    <input type="hidden" class="form-control" name = "idProducto" id="idProducto" value="<?=$resProductos->id_producto?>">
                    <input type="hidden" class="form-control" name = "idAlmacen" id="idAlmacen" value="<?=$resProductos->id_almacen?>">
                    <input type="number" class="form-control" name ="existenciasProducto" id="existenciasProducto" value="<?=$resProductos->existencias?>">
                </div>
                <div style="align-items: center;">
                    <button type="send" class="btn btn-success" style="margin-top: 10px;">Actualizar</button>
                </div>
            </form>
        <?
    }
    
    function muestraProductoAgregar($arrayAlmacenes){
        //var_dump($arrayAlmacenes);
        ?>
            <form action="app/controladores/cProducto.php?orden=guardarProducto" method="post" style="margin: 30px;"> 
            
            <div class="form-group">
                <label for="idAlmacen">Codigo de producto</label>
                <!--<input type="hidden" class="form-control" name = "idAlmacen" id="idAlmacen" value="<?=$idAlmacen?>">-->
                <input type="text" class="form-control" name = "codigoProducto" id="codigoProducto">
            </div>
            <div class="form-group">
                <label for="descripcionProducto">Descripcion de producto</label>
                <input type="text" class="form-control" name ="descripcionProducto" id="descripcionProducto" >
            </div>
            <div class="form-group">
                <label for="marcaProducto">Marca de producto</label>
                <input type="text" class="form-control" name="marcaProducto" id="marcaProducto">
            </div>
            <div class="form-group">
                <label for="colorProducto">Color de producto</label>
                <input type="text" class="form-control" name ="colorProducto" id="colorProducto">
            </div>
            <div class="form-group">
                <label for="precioProducto">Precio de producto</label>
                <input type="text" class="form-control" name ="precioProducto" id="precioProducto">
            </div>
            <div class="form-group">
                <label for="numeroExistencias">Numero existencias</label>
                <input type="number" class="form-control" name ="numeroExistencias" id="numeroExistencias">
            </div>
            <div class="form-group">
                <label for="tipoAlmacen">Tipo almacen</label>
            </div>
            <div class="form-group">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputAlmacen">Almacen</label>
                    </div>
                    <select class="custom-select" id="inputAlmacen" name="inputAlmacen">
                        <option selected>Seleccionar...</option>

                        <?
                            for($i = 0; $i < sizeof($arrayAlmacenes); $i++){
                                if($arrayAlmacenes[$i]['tipo'] == "t"){
                                ?>
                                        <option value="<?=$arrayAlmacenes[$i]['id_almacen']?>"><?=$arrayAlmacenes[$i]['nombre_almacen']." (Almacen fisico)"?></option>
                                <?     
                                }else{
                                    ?>
                                        <option value="<?=$arrayAlmacenes[$i]['id_almacen']?>"><?=$arrayAlmacenes[$i]['nombre_almacen']." (Almacen virtual)"?></option>
                                <? 
                                }
                            } 
                        ?>
                    </select>
                    </div>
                </div>
                <button type="send" class="btn btn-success">Guardar</button>
            </div>
        </form>
        <?
    }

    function muestraProductoAgregarAlmacenFisicos($arrayAlmacenes){
        ?>
            <form action="app/controladores/cProducto.php?orden=guardarProductoAlmacenFisico" method="post" style="margin: 30px;"> 
            
            <div class="form-group">
                <label for="idAlmacen">Codigo de producto</label>
                <input type="text" class="form-control" name = "codigoProducto" id="codigoProducto">
            </div>
            <div class="form-group">
                <label for="descripcionProducto">Descripcion de producto</label>
                <input type="text" class="form-control" name ="descripcionProducto" id="descripcionProducto" >
            </div>
            <div class="form-group">
                <label for="marcaProducto">Marca de producto</label>
                <input type="text" class="form-control" name="marcaProducto" id="marcaProducto">
            </div>
            <div class="form-group">
                <label for="colorProducto">Color de producto</label>
                <input type="text" class="form-control" name ="colorProducto" id="colorProducto">
            </div>
            <div class="form-group">
                <label for="precioProducto">Precio de producto</label>
                <input type="text" class="form-control" name ="precioProducto" id="precioProducto">
            </div>
            <div class="form-group">
                <label for="numeroExistencias">Numero existencias</label>
                <input type="number" class="form-control" name ="numeroExistencias" id="numeroExistencias">
            </div>
            <div class="form-group">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputAlmacen">Almacen</label>
                </div>
                <select class="custom-select" id="inputAlmacen" name="inputAlmacen">
                    <option selected>Seleccionar...</option>

                    <?
                        for($i = 0; $i < sizeof($arrayAlmacenes); $i++){
                            ?><option value="<?=$arrayAlmacenes[$i]['id_almacen']?>"><?=$arrayAlmacenes[$i]['nombre_almacen']?></option><?
                        } 
                    ?>
                </select>
                </div>
            </div>
            <div>
                <button type="send" class="btn btn-success">Guardar</button>
            </div>
        </form>
        <?    
    }

    function muestraProductoAgregarAlmacenVirtual($arrayAlmacenes){
        ?>
            <form action="app/controladores/cProducto.php?orden=guardarProductoAlmacenVirtual" method="post" style="margin: 30px;"> 
            
            <div class="form-group">
                <label for="idAlmacen">Codigo de producto</label>
                <input type="text" class="form-control" name = "codigoProducto" id="codigoProducto">
            </div>
            <div class="form-group">
                <label for="descripcionProducto">Descripcion de producto</label>
                <input type="text" class="form-control" name ="descripcionProducto" id="descripcionProducto" >
            </div>
            <div class="form-group">
                <label for="marcaProducto">Marca de producto</label>
                <input type="text" class="form-control" name="marcaProducto" id="marcaProducto">
            </div>
            <div class="form-group">
                <label for="colorProducto">Color de producto</label>
                <input type="text" class="form-control" name ="colorProducto" id="colorProducto">
            </div>
            <div class="form-group">
                <label for="precioProducto">Precio de producto</label>
                <input type="text" class="form-control" name ="precioProducto" id="precioProducto">
            </div>
            <div class="form-group">
                <label for="numeroExistencias">Numero existencias</label>
                <input type="number" class="form-control" name ="numeroExistencias" id="numeroExistencias">
            </div>
            <div class="form-group">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputAlmacen">Almacen</label>
                </div>
                <select class="custom-select" id="inputAlmacen" name="inputAlmacen">
                    <option selected>Seleccionar...</option>

                    <?
                        for($i = 0; $i < sizeof($arrayAlmacenes); $i++){
                            ?><option value="<?=$arrayAlmacenes[$i]['id_almacen']?>"><?=$arrayAlmacenes[$i]['nombre_almacen']?></option><?
                        } 
                    ?>
                </select>
                </div>
            </div>
            <div>
                <button type="send" class="btn btn-success">Guardar</button>
            </div>
        </form>
        <?  
    }
}
?>
