-- Database: productos

-- DROP DATABASE productos;

CREATE DATABASE productos
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Spain.1252'
    LC_CTYPE = 'Spanish_Spain.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	
CREATE TABLE producto (
	id_producto serial PRIMARY KEY,
	sku varchar (15) UNIQUE NOT NULL,
	descripcion text NOT NULL,
	marca varchar(30) NOT NULL,
	color varchar(15) NOT NULL,
	precio decimal NOT NULL
);

CREATE TABLE almacen (
	id_almacen serial PRIMARY KEY,
	nombre_almacen varchar (30) UNIQUE NOT NULL,
	localizacion varchar(30) NOT NULL,
	responsable varchar(50) NOT NULL,
	tipo boolean default false
);

CREATE TABLE existencias(
	id_existencias serial PRIMARY KEY,
	id_producto integer REFERENCES producto(id_producto) NOT NULL,
	id_almacen integer REFERENCES almacen (id_almacen) NOT NULL,
	existencias integer null
);

INSERT INTO producto
  (sku, descripcion, marca, color, precio)
VALUES
  ('000000000000001', 'Balsamo para el cuidado de la barba, diseñado para el manejo de la barba.', 'Beardbrand', 'rojo', 349.99), 
  ('000000000000002', 'Aceite para la barba de 50ml, olor a coco.', 'Beardbrand', 'cafe', 225.00), 
  ('000000000000003', 'Peina de madera para la barba. Tamaño 15 x 10cm', 'Beardbrand', 'cafe', 150.00),
  ('000000000000004', 'Crema para la barba.', 'Beardbrand', 'rojo', 369.00),
  ('000000000000005', 'Shampoo para la barba, tamño de 350ml.', 'Beardbrand', 'verde', 369.00),
  ('000000000000006', 'Tenis Modelo 544881df', 'Puma', 'blanco/negro', 1550.00),
  ('000000000000007', 'Tenis Modelo 541ss1dg', 'Nike', 'blanco/azul', 2550.00),
  ('000000000000008', 'Tenis Modelo 587881dk', 'Reebok', 'negro', 1900.00), 
  ('000000000000009', 'Tenis Modelo 544000df', 'New Balance', 'negro', 3500.50),
  ('000000000000010', 'Tenis Modelo 6965kl1d', 'Converse', 'blanco', 999.99),
  ('000000000000011', 'Playera New Era', 'New Era', 'blanco', 999.99),
  ('000000000000012', 'Pantalon Levis', 'Levis', 'negro', 799.99),
  ('000000000000013', 'Paquete de boxers', 'Hugo Boss', 'blanco', 499.99),
  ('000000000000014', 'Pantalon', 'Levis', 'blanco', 858.99),
  ('000000000000015', 'Perfume Carlo Corintio de 100ml', 'Carlo Corintio', 'azul', 1200.50);

INSERT INTO almacen
  	(nombre_almacen, localizacion, responsable, tipo)
VALUES
	('Monterrey', 'Norte', 'Milos Adrian', true),
	('Veracruz', 'Este', 'Jose Antonio', false),
	('CDMX', 'centro', 'Roberto Toledo', false),
	('Tabasco', 'sur', 'Angel Rugerio', true);
	

INSERT INTO existencias
	(id_producto, id_almacen, existencias)
VALUES
	(1, 1, 20),
	(2, 3, 50),
	(3, 1, 35),
	(4, 2, 22),
	(5, 4, 95),
	(6, 1, 10),
	(7, 3, 5),
	(8, 4, 10),
	(9, 2, 12),
	(10, 1, 100),
	(11, 3, 4),
	(12, 1, 80),
	(13, 2, 1),
	(14, 2, 10),
	(15, 3, 2),
	(15, 1, 25),
	(11, 3, 20),
	(5, 4, 100),
	(9, 2, 50),
	(11, 2, 10);
	
	
	
  
  
  
  
  
  
  
  
  
  
  
  
  
  