

var alertList = document.querySelectorAll('.alert')
alertList.forEach(function (alert) {
  new bootstrap.Alert(alert)
})


function mostrarExistenciasFisicas($idProducto){
    var id = $idProducto;
    //window.location='../Productos/app/controladores/cProducto.php?orden=mostrarExistenciasFisicas&id='+id;
    window.location='../Productos/productos-almacen-fisico.php?orden=mostrarExistenciasFisicas&id='+id;
}



function mostrarExistenciasVirtuales($idProducto){
    var id = $idProducto;
    window.location='../Productos/productos-almacen-virtual.php?orden=mostrarExistenciasVirtuales&id='+id;
}

function editarProducto($idProducto){
    var id = $idProducto;
    window.location='../Productos/editar-producto.php?orden=editarProducto&id='+id;
}

function eliminarProducto($idProducto){
    var id = $idProducto;
    
    var opcion = confirm("¿Esta seguro que quiere eliminar el producto de todos los almacenes?");
        if (opcion == true) {
            window.location='../Productos/app/controladores/cProducto.php?orden=eliminarProducto&id='+id;
        } else {
            
        }
}

function eliminarProductoFisico($idProducto){
    var id = $idProducto;
    
    var opcion = confirm("¿Esta seguro que quiere eliminar el producto del almacen fisico?");
        if (opcion == true) {
            window.location='../Productos/app/controladores/cProducto.php?orden=eliminarProductoFisico&id='+id;
        } else {
            
        }
}

function editarExistenciasFisicas($idProducto){
    var id = $idProducto;
    window.location='../Productos/editar-producto-existencias.php?orden=editarProductoExistencias&id='+id;
}

function editarExistenciasVirtuales($idProducto){
    var id = $idProducto;
    window.location='../Productos/editar-producto-existencias-virtuales.php?orden=editarProductoExistenciasVirtuales&id='+id;
}

function agregarProductoAlmacen($idAlmacen){
    var idAlmacen = $idAlmacen;
    window.location='../Productos/agregar-producto-almacen.php?orden=agregarProductoXalmacen&idAlmacen='+idAlmacen;
}

function agregarProductoAlmacenFisico(){
    window.location='../Productos/agregar-producto-almacen-fisico.php?orden=agregarProductoAlmacenFisico';
}

function agregarProductoAlmacenVirtual(){
    window.location='../Productos/agregar-producto-almacen-virtual.php?orden=agregarProductoAlmacenVirtual';
}



