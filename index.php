<?
include_once("./app/sistema/rutas.php");
?>
<!DOCTYPE html>
<html lang="en">    
    <head>
    <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Productos</title>

        <!-- CSS de Bootstrap -->
        <link href="<?=$raizComponentes?>bootstrap-5.0.0-beta3-dist/css/bootstrap.min.css" rel="stylesheet" media="screen">

        <!--Estilos Propios-->
        <link href="<?=$raizEstilos?>productos.css" rel="stylesheet" media="screen">

        <!-- Componentes de JS -->
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

        <script type="application/javascript" src="<?=$raizComponentes?>jquery/jquery-3.6.0.min.js"></script>
        <script href="<?=$raizComponentes?>bootstrap-5.0.0-beta3-dist/js/bootstrap.min.js" rel="stylesheet" media="screen"></script>
        <!--Propias-->
        <script type="application/javascript" src="<?=$raizJs?>funciones.js"></script>
    </head>
    <body class="body-content">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                    <a class="navbar-brand" href="#"><strong>Productos</strong></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="./index.php?orden=muestraProductos">Todos</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="./index.php?orden=muestraProductosFisicos">Físicos</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="./index.php?orden=muestraProductosVirtuales">Virtuales</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="./nuevo-producto.php?orden=agregarProductoXalmacen"><strong>Nuevo producto</strong></a>
                        </li>
                    </ul>
                    </div>
            </div>
        </nav>
        <div class="container" style="margin-top: 25px;">

            <?  
                if(isset($_REQUEST['mensaje'])){
                    $msj = $_REQUEST['mensaje'];

                    if(!empty($msj)){
                        if($msj == "guardar"){
                            ?>
                                <div class="alert alert-info alert-dismissible fade show" role="alert">
                                    <h6 style="text-align: center;">Producto guardado</h6> 
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            <?
                        }

                        if($msj == "actualizar"){
                            ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <h6 style="text-align: center;">Producto actualizado</h6> 
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            <?
                        }

                        if($msj == "eliminar"){
                            ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <h6 style="text-align: center;">Producto eliminado</h6> 
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            <?
                        }
                    }else{
                        
                    }
                }

                require_once($_SERVER['DOCUMENT_ROOT']."/Productos/app/controladores/cProducto.php");
            ?>
        </div>       
    </body>
</html>